package de.turing85.customer;

import de.turing85.customer.boundary.transfer.CustomerModel;
import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.event.Event;
import javax.inject.Inject;
import javax.transaction.Transactional;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;

@ApplicationScoped
public class CustomerService {
  private final Event<CustomerModel> customerCreatedEvent;

  @Inject
  public CustomerService(@CustomerCreated Event<CustomerModel> customerCreatedEvent) {
    this.customerCreatedEvent = customerCreatedEvent;
  }

  @Transactional
  public CustomerModel createCustomer(@Valid @NotNull CustomerModel customer) {
    customerCreatedEvent.fire(customer);
    return customer;
  }
}