package de.turing85.customer.boundary.messaging.outgoing;

import de.turing85.customer.CustomerCreated;
import de.turing85.customer.boundary.transfer.CustomerModel;
import io.smallrye.reactive.messaging.kafka.OutgoingKafkaRecordMetadata;
import javax.enterprise.event.Observes;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import org.eclipse.microprofile.reactive.messaging.Channel;
import org.eclipse.microprofile.reactive.messaging.Emitter;
import org.eclipse.microprofile.reactive.messaging.Message;
import org.slf4j.Logger;

public class KafkaCustomerCreatedEmitter {
  public static final String CUSTOMER_CREATED_CHANNEL = "customer-created-channel";
  private final Emitter<CustomerModel> emitter;
  private final Logger logger;

  public KafkaCustomerCreatedEmitter(
      @Channel(CUSTOMER_CREATED_CHANNEL) Emitter<CustomerModel> emitter,
      Logger logger) {
    this.emitter = emitter;
    this.logger = logger;
  }

  public void emit(@Valid @NotNull @Observes @CustomerCreated CustomerModel customer) {
    logger.info("Sending \"{}\" to channel \"{}\"", customer, CUSTOMER_CREATED_CHANNEL);
    emitter.send(Message.of(customer)
        .addMetadata(OutgoingKafkaRecordMetadata.<String>builder()
            .withKey(customer.getName())
            .build()));
  }
}