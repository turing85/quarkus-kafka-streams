package de.turing85.customer.boundary.endpoint;

import de.turing85.customer.CustomerService;
import de.turing85.customer.boundary.transfer.CustomerModel;
import java.net.URI;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import lombok.RequiredArgsConstructor;

@Path("/api/customers")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
@RequiredArgsConstructor
public class CustomerEndpoint {
  private final CustomerService service;

  @POST
  public Response createNewCustomer(@Valid @NotNull CustomerModel customer) {
    return Response.created(URI.create(customer.getName()))
        .entity(service.createCustomer(customer))
        .build();
  }
}