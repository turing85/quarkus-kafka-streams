package de.turing85.customer.boundary.transfer;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Value;
import lombok.extern.jackson.Jacksonized;

@Value
@Builder
@Jacksonized
@AllArgsConstructor
public class CustomerModel {
  @NotEmpty
  @Size(min = 3, max = 63)
  String name;
}