package de.turing.aggregator.simple.boundary.transfer;

import io.quarkus.runtime.annotations.RegisterForReflection;
import lombok.Builder;
import lombok.Value;

@RegisterForReflection
@Value
@Builder(toBuilder = true)
public class
OrderCounterModel {
  int counter;
}