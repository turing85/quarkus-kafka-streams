package de.turing.aggregator.simple.boundary.messaging.topology;

import com.fasterxml.jackson.databind.ObjectMapper;
import de.turing.aggregator.simple.boundary.transfer.CustomerModel;
import de.turing.aggregator.simple.boundary.transfer.OrderCounterModel;
import de.turing.aggregator.simple.boundary.transfer.OrderModel;
import io.quarkus.kafka.client.serialization.ObjectMapperSerde;
import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.inject.Produces;
import org.apache.kafka.common.serialization.Serdes;
import org.apache.kafka.streams.StreamsBuilder;
import org.apache.kafka.streams.Topology;
import org.apache.kafka.streams.kstream.Consumed;
import org.apache.kafka.streams.kstream.Materialized;
import org.apache.kafka.streams.kstream.Produced;
import org.apache.kafka.streams.state.KeyValueBytesStoreSupplier;
import org.apache.kafka.streams.state.Stores;

@ApplicationScoped
public class TopologyProducer {
  private static final String ORDER_CREATED_CHANNEL = "new-order";
  private static final String CUSTOMER_CREATED_CHANNEL = "new-customer";
  private static final String CUSTOMER_TABLE = "customer-table";
  private static final String CUSTOMER_ORDER_COUNTER_CHANNEL = "customer-order-counter-channel";
  private static final String CUSTOMER_COUNT_TABLE = "customer-count-table";

  private final ObjectMapperSerde<CustomerModel> customerSerde;
  private final ObjectMapperSerde<OrderModel> orderModelObjectMapperSerde;
  private final ObjectMapperSerde<OrderCounterModel> customerOrderSerde;

  public TopologyProducer(ObjectMapper objectMapper) {
    customerSerde = new ObjectMapperSerde<>(CustomerModel.class, objectMapper);
    orderModelObjectMapperSerde = new ObjectMapperSerde<>(OrderModel.class, objectMapper);
    customerOrderSerde = new ObjectMapperSerde<>(OrderCounterModel.class, objectMapper);
  }

  @Produces
  public Topology buildTopology() {
    final StreamsBuilder builder = new StreamsBuilder();
    final KeyValueBytesStoreSupplier storeSupplier = Stores.inMemoryKeyValueStore(CUSTOMER_TABLE);
    builder
        .stream(ORDER_CREATED_CHANNEL, Consumed.with(Serdes.String(), orderModelObjectMapperSerde))
        .join(
            builder.table(
                CUSTOMER_CREATED_CHANNEL,
                Materialized.<String, CustomerModel>as(storeSupplier)
                    .withKeySerde(Serdes.String())
                    .withValueSerde(customerSerde)),
            (order, customer) -> 1)
        .groupByKey()
        .count(Materialized.as(Stores.inMemoryKeyValueStore(CUSTOMER_COUNT_TABLE)))
        .toStream()
        .mapValues((customerName, counter) -> OrderCounterModel.builder()
            .counter(counter.intValue())
            .build())
        .to(CUSTOMER_ORDER_COUNTER_CHANNEL, Produced.with(Serdes.String(), customerOrderSerde));
    return builder.build();
  }
}