package de.turing.aggregator.simple.boundary.transfer;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Value;

@Value
@Builder
@AllArgsConstructor
public class OrderModel {
  int priceInCents;
}