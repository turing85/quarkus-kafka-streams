package de.turing85.order.boundary.messaging.outgoing;

import de.turing85.order.CustomerService;
import de.turing85.order.OrderService;
import de.turing85.order.boundary.transfer.CustomerModel;
import de.turing85.order.boundary.transfer.OrderModel;
import io.smallrye.mutiny.Multi;
import io.smallrye.reactive.messaging.kafka.OutgoingKafkaRecordMetadata;
import java.time.Duration;
import java.util.Optional;
import javax.enterprise.context.ApplicationScoped;
import lombok.RequiredArgsConstructor;
import org.eclipse.microprofile.reactive.messaging.Message;
import org.eclipse.microprofile.reactive.messaging.Outgoing;
import org.slf4j.Logger;

@ApplicationScoped
@RequiredArgsConstructor
public class KafkaOrderGenerator {
  public static final String ORDER_CREATED_CHANNEL = "order-created-channel";

  private final OrderService orderService;
  private final CustomerService customerService;
  private final Logger logger;

  @Outgoing(ORDER_CREATED_CHANNEL)
  public Multi<Message<OrderModel>> generate() {
    return Multi.createFrom().ticks().every(Duration.ofMillis(500))
        .onOverflow().drop()
        .map(tick -> orderService.createRandomOrder())
        .filter(Optional::isPresent)
        .map(Optional::get)
        .invoke(order -> logger.info(
            "Sending \"{}\" to channel \"{}\"",
            order,
            ORDER_CREATED_CHANNEL))
        .map(this::toMessage);
  }

  private Message<OrderModel> toMessage(OrderModel orderModel) {
    return Message.of(orderModel)
        .addMetadata(OutgoingKafkaRecordMetadata.<String>builder()
            .withKey(customerService.findRandomCustomer().map(CustomerModel::getName).orElse(null))
            .withTimestamp(orderModel.getCreated())
            .build());
  }
}