package de.turing85.order.boundary.transfer;

import com.fasterxml.jackson.annotation.JsonIgnore;
import java.time.Instant;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Value;

@Value
@Builder
@AllArgsConstructor
public class OrderModel {
  int priceInCents;
  @JsonIgnore
  Instant created;
}