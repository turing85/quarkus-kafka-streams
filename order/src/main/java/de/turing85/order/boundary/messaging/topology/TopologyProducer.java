package de.turing85.order.boundary.messaging.topology;

import com.fasterxml.jackson.databind.ObjectMapper;
import de.turing85.order.boundary.transfer.CustomerModel;
import io.quarkus.kafka.client.serialization.ObjectMapperSerde;
import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.inject.Produces;
import org.apache.kafka.common.serialization.Serdes;
import org.apache.kafka.streams.StreamsBuilder;
import org.apache.kafka.streams.Topology;
import org.apache.kafka.streams.kstream.Materialized;
import org.apache.kafka.streams.state.KeyValueBytesStoreSupplier;
import org.apache.kafka.streams.state.Stores;

@ApplicationScoped
public class TopologyProducer {
  private static final String NEW_CUSTOMER_CHANNEL = "new-customer";
  public static final String CUSTOMER_TABLE = "customer-table";

  private final ObjectMapperSerde<CustomerModel> customerSerde;

  public TopologyProducer(ObjectMapper objectMapper) {
    customerSerde = new ObjectMapperSerde<>(CustomerModel.class, objectMapper);
  }

  @Produces
  public Topology buildTopology() {
    final StreamsBuilder builder = new StreamsBuilder();
    final KeyValueBytesStoreSupplier customerTableSupplier =
        Stores.inMemoryKeyValueStore(CUSTOMER_TABLE);
    builder.table(
        NEW_CUSTOMER_CHANNEL,
        Materialized.<String, CustomerModel>as(customerTableSupplier)
            .withKeySerde(Serdes.String())
            .withValueSerde(customerSerde));
    return builder.build();
  }
}