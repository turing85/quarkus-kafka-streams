package de.turing85.order.boundary.persistence;

import de.turing85.order.boundary.messaging.topology.TopologyProducer;
import de.turing85.order.boundary.transfer.CustomerModel;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Spliterator;
import java.util.Spliterators;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import java.util.stream.StreamSupport;
import javax.enterprise.context.ApplicationScoped;
import lombok.RequiredArgsConstructor;
import org.apache.kafka.streams.KafkaStreams;
import org.apache.kafka.streams.StoreQueryParameters;
import org.apache.kafka.streams.state.QueryableStoreTypes;
import org.apache.kafka.streams.state.ReadOnlyKeyValueStore;

@ApplicationScoped
@RequiredArgsConstructor
public class CustomerRepository {
  private final KafkaStreams streams;

  public List<CustomerModel> findAll() {
    try {
      return iteratorToStream(getCustomerTableStore().all())
          .map(keyValue -> keyValue.value)
          .collect(Collectors.toList());
    } catch (Exception e) {
      return Collections.emptyList();
    }
  }

  private ReadOnlyKeyValueStore<String, CustomerModel> getCustomerTableStore() {
    return streams.store(StoreQueryParameters.fromNameAndType(
        TopologyProducer.CUSTOMER_TABLE,
        QueryableStoreTypes.keyValueStore()));
  }

  private static <T> Stream<T> iteratorToStream(Iterator<T> iterator) {
    return StreamSupport.stream(
        Spliterators.spliteratorUnknownSize(iterator, Spliterator.ORDERED),
        false);
  }
}