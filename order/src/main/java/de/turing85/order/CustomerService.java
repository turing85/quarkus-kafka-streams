package de.turing85.order;

import de.turing85.order.boundary.persistence.CustomerRepository;
import de.turing85.order.boundary.transfer.CustomerModel;
import java.util.List;
import java.util.Optional;
import java.util.Random;
import javax.enterprise.context.ApplicationScoped;
import lombok.RequiredArgsConstructor;

@ApplicationScoped
@RequiredArgsConstructor
public class CustomerService {
  private static final Random RANDOM = new Random();

  private final CustomerRepository repository;

  public Optional<CustomerModel> findRandomCustomer() {
    final List<CustomerModel> allCustomers = repository.findAll();
    if (allCustomers.isEmpty()) {
      return Optional.empty();
    }
    return Optional.of(allCustomers.get(RANDOM.nextInt(allCustomers.size())));
  }
}