package de.turing85.order;

import de.turing85.order.boundary.transfer.OrderModel;
import java.time.Instant;
import java.util.Optional;
import java.util.Random;
import javax.enterprise.context.ApplicationScoped;
import lombok.RequiredArgsConstructor;

@ApplicationScoped
@RequiredArgsConstructor
public class OrderService {
  private static final Random RANDOM = new Random();

  private final CustomerService customerService;

  public Optional<OrderModel> createRandomOrder() {
    return customerService.findRandomCustomer()
      .map(customer -> OrderModel.builder()
        .priceInCents(RANDOM.nextInt(90_00) + 10)
        .created(Instant.now())
        .build());
  }
}