package de.turing.aggregator.superb.boundary.persistence;

import de.turing.aggregator.superb.boundary.messaging.topology.TopologyProducer;
import de.turing.aggregator.superb.boundary.transfer.CustomerOrderWindowedStatistics;
import de.turing.aggregator.superb.boundary.transfer.OrderStatistics;
import java.time.Instant;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;
import java.util.Spliterator;
import java.util.Spliterators;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import java.util.stream.StreamSupport;
import javax.enterprise.context.ApplicationScoped;
import lombok.RequiredArgsConstructor;
import org.apache.kafka.streams.KafkaStreams;
import org.apache.kafka.streams.KeyValue;
import org.apache.kafka.streams.StoreQueryParameters;
import org.apache.kafka.streams.kstream.Windowed;
import org.apache.kafka.streams.state.QueryableStoreTypes;
import org.apache.kafka.streams.state.ReadOnlyWindowStore;

@ApplicationScoped
@RequiredArgsConstructor
public class CustomerOrderStatisticsRepository {
  private final KafkaStreams streams;

  public List<CustomerOrderWindowedStatistics> findAll() {
    return iteratorToStream(readOnlyStatisticsStore().all())
        .map(this::keyValueToCustomerOrderWindowedStatistics)
        .sorted(Comparator.comparing(CustomerOrderWindowedStatistics::getStartTime))
        .collect(Collectors.toList());
  }

  private ReadOnlyWindowStore<String, OrderStatistics> readOnlyStatisticsStore() {
    return streams.store(StoreQueryParameters.fromNameAndType(
        TopologyProducer.CUSTOMER_ORDER_STATISTICS_TABLE,
        QueryableStoreTypes.windowStore()));
  }

  private static <T> Stream<T> iteratorToStream(Iterator<T> iterator) {
    return StreamSupport.stream(
        Spliterators.spliteratorUnknownSize(iterator, Spliterator.ORDERED),
        false);
  }

  private CustomerOrderWindowedStatistics keyValueToCustomerOrderWindowedStatistics(
      KeyValue<Windowed<String>, OrderStatistics> keyValue) {
    return CustomerOrderWindowedStatistics.builder()
        .startTime(keyValue.key.window().startTime())
        .customerName(keyValue.key.key())
        .endTime(keyValue.key.window().endTime())
        .orderStatistics(keyValue.value)
        .build();
  }

  public List<CustomerOrderWindowedStatistics> findAll(Instant timeFrom, Instant timeTo) {
    return iteratorToStream(readOnlyStatisticsStore().fetchAll(timeFrom, timeTo))
        .map(this::keyValueToCustomerOrderWindowedStatistics)
        .sorted(Comparator.comparing(CustomerOrderWindowedStatistics::getStartTime))
        .collect(Collectors.toList());
  }
}