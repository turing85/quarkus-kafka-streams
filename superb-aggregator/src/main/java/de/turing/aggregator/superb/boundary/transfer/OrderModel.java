package de.turing.aggregator.superb.boundary.transfer;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Value;

@Value
@Builder
@AllArgsConstructor
public class OrderModel {
  int priceInCents;
}