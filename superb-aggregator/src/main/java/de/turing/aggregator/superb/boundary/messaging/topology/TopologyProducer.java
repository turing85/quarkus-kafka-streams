package de.turing.aggregator.superb.boundary.messaging.topology;

import com.fasterxml.jackson.databind.ObjectMapper;
import de.turing.aggregator.superb.boundary.transfer.CustomerModel;
import de.turing.aggregator.superb.boundary.transfer.OrderModel;
import de.turing.aggregator.superb.boundary.transfer.OrderStatistics;
import de.turing.aggregator.superb.boundary.transfer.OrderWindowedStatistics;
import io.quarkus.kafka.client.serialization.ObjectMapperSerde;
import java.time.Duration;
import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.inject.Produces;
import javax.inject.Inject;
import org.apache.kafka.common.serialization.Serdes;
import org.apache.kafka.streams.StreamsBuilder;
import org.apache.kafka.streams.Topology;
import org.apache.kafka.streams.kstream.Consumed;
import org.apache.kafka.streams.kstream.Materialized;
import org.apache.kafka.streams.kstream.Produced;
import org.apache.kafka.streams.kstream.SlidingWindows;
import org.apache.kafka.streams.state.Stores;

@ApplicationScoped
public class TopologyProducer {

  public static final String ORDER_CREATED_CHANNEL = "new-order";
  private static final String CUSTOMER_TABLE = "customer-table";
  public static final String CUSTOMER_ORDER_STATISTICS_TABLE = "customer-order-statistics-table";
  private static final String CUSTOMER_CREATED_CHANNEL = "new-customer";
  private static final String CUSTOMER_ORDER_COUNTER_WINDOWED_CHANNEL =
      "customer-order-counter-windowed-channel";
  private static final Duration RETENTION_PERIOD = Duration.ofMinutes(5);
  private static final Duration WINDOW_SIZE = Duration.ofMinutes(1);
  private static final Duration WINDOW_GRACE = Duration.ofMillis(250);

  private final ObjectMapperSerde<CustomerModel> customerSerde;
  private final ObjectMapperSerde<OrderModel> orderSerde;
  private final ObjectMapperSerde<OrderStatistics> customerOrderStatisticsSerde;
  private final ObjectMapperSerde<OrderWindowedStatistics> customerOrderWindowedStatisticsSerde;

  @Inject
  public TopologyProducer(ObjectMapper objectMapper) {
    customerSerde = new ObjectMapperSerde<>(CustomerModel.class, objectMapper);
    orderSerde = new ObjectMapperSerde<>(OrderModel.class, objectMapper);
    customerOrderStatisticsSerde =
        new ObjectMapperSerde<>(OrderStatistics.class, objectMapper);
    customerOrderWindowedStatisticsSerde =
        new ObjectMapperSerde<>(OrderWindowedStatistics.class, objectMapper);
  }

  @Produces
  public Topology buildTopology() {
    final StreamsBuilder builder = new StreamsBuilder();
    builder.stream(ORDER_CREATED_CHANNEL, Consumed.with(Serdes.String(), orderSerde))
        .join(
            builder.table(
                CUSTOMER_CREATED_CHANNEL,
                Materialized.<String, CustomerModel>as(Stores.inMemoryKeyValueStore(CUSTOMER_TABLE))
                    .withKeySerde(Serdes.String())
                    .withValueSerde(customerSerde)),
            (order, customer) -> order)
        .groupByKey()
        .windowedBy(SlidingWindows.withTimeDifferenceAndGrace(WINDOW_SIZE, WINDOW_GRACE))
        .aggregate(
            OrderStatistics::new,
            (customerName, order, stats) -> stats
                .incrementNumOrders()
                .addToTotalCostInCents(order.getPriceInCents()),
            Materialized.<String, OrderStatistics>as(Stores
                .inMemoryWindowStore(
                    CUSTOMER_ORDER_STATISTICS_TABLE,
                    RETENTION_PERIOD,
                    WINDOW_SIZE,
                    false))
                .withKeySerde(Serdes.String())
                .withValueSerde(customerOrderStatisticsSerde))
        .mapValues((windowedCustomerName, customerOrderStatistics) ->
            OrderWindowedStatistics.builder()
                .startTime(windowedCustomerName.window().startTime())
                .endTime(windowedCustomerName.window().endTime())
                .orderStatistics(customerOrderStatistics)
                .build())
        .toStream((windowedCustomerName, windowedStatistics) -> windowedCustomerName.key())
        .to(
            CUSTOMER_ORDER_COUNTER_WINDOWED_CHANNEL,
            Produced.with(Serdes.String(), customerOrderWindowedStatisticsSerde));
    return builder.build();
  }
}