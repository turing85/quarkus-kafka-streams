package de.turing.aggregator.superb.boundary.transfer;

import com.fasterxml.jackson.annotation.JsonIgnore;
import java.time.Instant;
import lombok.Builder;
import lombok.Value;
import lombok.experimental.Delegate;
import lombok.extern.jackson.Jacksonized;

@Value
@Jacksonized
@Builder
public class OrderWindowedStatistics {
  @JsonIgnore
  @Delegate
  OrderStatistics orderStatistics;
  Instant startTime;
  Instant endTime;
  String customerName;
}