package de.turing.aggregator.superb.boundary.transfer;

import io.quarkus.runtime.annotations.RegisterForReflection;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Value;

@RegisterForReflection
@Value
@Builder
@AllArgsConstructor
public class CustomerModel {
  String name;
}