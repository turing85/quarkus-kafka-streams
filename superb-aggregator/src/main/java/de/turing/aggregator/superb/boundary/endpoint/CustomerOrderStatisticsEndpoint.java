package de.turing.aggregator.superb.boundary.endpoint;

import de.turing.aggregator.superb.boundary.CustomerOrderStatisticsService;
import java.time.Instant;
import java.util.Optional;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import lombok.RequiredArgsConstructor;

@Path("/api/statistics")
@Produces(MediaType.APPLICATION_JSON)
@RequiredArgsConstructor
public class CustomerOrderStatisticsEndpoint {
  private final CustomerOrderStatisticsService service;

  @GET
  public Response find(
      @QueryParam("from") Optional<String> fromString,
      @QueryParam("to") Optional<String> toString) {
    return Response
        .ok(service.findAll(
            fromString.map(Instant::parse).orElse(null),
            toString.map(Instant::parse).orElse(null)))
        .build();
  }
}