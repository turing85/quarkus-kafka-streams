package de.turing.aggregator.superb.boundary;

import de.turing.aggregator.superb.boundary.persistence.CustomerOrderStatisticsRepository;
import de.turing.aggregator.superb.boundary.transfer.CustomerOrderWindowedStatistics;
import java.time.Instant;
import java.util.List;
import java.util.Optional;
import javax.enterprise.context.ApplicationScoped;
import lombok.RequiredArgsConstructor;

@ApplicationScoped
@RequiredArgsConstructor
public class CustomerOrderStatisticsService {
  private final CustomerOrderStatisticsRepository repository;

  public List<CustomerOrderWindowedStatistics> findAll(Instant timeFrom, Instant timeTo) {
    final Optional<Instant> maybeTimeFrom = Optional.ofNullable(timeFrom);
    final Optional<Instant> maybeTimeTo = Optional.ofNullable(timeTo);
    if (maybeTimeFrom.isEmpty() && maybeTimeTo.isEmpty()) {
      return repository.findAll();
    }
    return repository.findAll(
        maybeTimeFrom.orElse(Instant.ofEpochMilli(0)),
        maybeTimeTo.orElse(Instant.now()));
  }
}