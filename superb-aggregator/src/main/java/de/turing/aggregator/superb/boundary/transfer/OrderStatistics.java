package de.turing.aggregator.superb.boundary.transfer;

import lombok.Getter;

@Getter
public class OrderStatistics {
  private int numOrders;
  private int totalCostInCents;

  public OrderStatistics incrementNumOrders() {
    ++numOrders;
    return this;
  }

  public OrderStatistics addToTotalCostInCents(int costInCents) {
    totalCostInCents += costInCents;
    return this;
  }
}